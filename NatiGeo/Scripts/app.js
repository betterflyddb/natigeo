﻿//window.addEventListener("keydown", function (e) {
//    // space and arrow keys
//    //if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
//    //    e.preventDefault();
//    if ([ 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
//           e.preventDefault();
//    }
//}, false);
var Time = 15;
window.onkeydown = function (e) {
    if (e.keyCode == 32 && e.target == document.body) {
        e.preventDefault();
        return false;
    }
};

function onscreenresize() {
    var ScreenHeight = $(window).height();
    var ScreebWidth = $(window).width();
    var DivPointer = $("#MainDiv");
    var DivWidth = 1920;
    var DivHeith = 1080;
    var bottom = (DivHeith - ScreenHeight) / 2;
    var right = (DivWidth - ScreebWidth) / 2;
    DivPointer.css("bottom", bottom);
    DivPointer.css("right", right);
}
$(onscreenresize);
$(window).resize(onscreenresize);
$(".videoICon").mouseover(function () {
    $(this).addClass("VideoiConActive");
    var $this = $(this).parent();
    $this.css("z-index", "700");
    $this.find(".VideoHideenWrapper").fadeIn();
});
$(".WholeVideoWrapper").mouseleave(function () {
    {
        var $this = $(this);

        $this.find(".VideoHideenWrapper").fadeOut();
        $this.css("z-index", "200");
        $(this).find(".videoICon").removeClass("VideoiConActive");

    }
});
$(document).on("click", ".PopUpCloseBut", function () {

    $(".PopUp").remove();
    $(".PopUpBack").hide();

});
$(document).on("click", "#Start", function () {
    var html = $("#LoginPopUpTemplate").html();
    $(".PopUpBack").show();
    $("body").append(html);
});
$(".TubnailWrapper").click(function () {
    var $this = $(this).parent();
    var Template = $("#VideoTemplate").html();
    var Url = $this.data("url");
    Template = Template.replace("{Url}", Url);
    $(".PopUpBack").show();
    $("body").append(Template);


});
$(document).on("click", "#loginSub", function () {
    var $this = $(this).parent().parent().parent();
    var Data = $this.find("form").serialize();
    $.ajax({
        tradional: true,
        url: U + 'ajax/ChackUser',
        data: Data,
        processData: false,
        type: 'POST',
        success: function (response) {
            if (response.Status) {
                switch (response.Data) {
                    case 0:
                        {
                            $(".PopUp").html($("#EnterDeteilsTemplate").html());
                        }
                        break;
                    case 1:
                        {
                            $(".PopUp").html($("#ChouseGiftTemplate").html());
                        }
                        break;
                }
            }
            else {
                switch (response.Data) {
                    case 0:
                        {
                            //Useri ver moidzebna
                            $(".LoginWrapper").addClass("error1");
                        }
                        break;
                    case 1:
                        {
                            //Users ar aqvs tv Momsaxureba
                        }
                        break;
                    case 2:
                        {

                            $(".PopUp").html($("#UpselingTemplate").html());
                        }
                        break;
                    case 3:
                        {
                            //ukve natmashebi aqvs 
                            $(".PopUp").html($("#RePlay").html());

                            break;
                        }


                }
            }
        }
    });

});
$(document).on("click", "#EditionalSend", function () {
    var Data = $("#EditionalInfo").serialize();
    $.ajax({
        tradional: true,
        url: U + 'ajax/Editional',
        data: Data,
        processData: false,
        type: 'POST',
        success: function (response) {
            if (!response.Status)
            {
                $(".LoginWrapper").addClass("error1");
                return;
            }

            $(".PopUp").html($("#ChouseGiftTemplate").html());
        }
    });
});
$(document).on("click", ".GiftWrapper", function () {
    var $this = $(this);
    $this.parent().find(".ActiveGift").removeClass("ActiveGift");
    $this.addClass("ActiveGift");
});
$(document).on("click", "#SaveGift", function () {
    var $this = $(this);
    var Data = new FormData();
    var id = $this.parent().parent().find(".ActiveGift").data("id");
    if (id == null) {
        $this.parent().addClass("error");
        return;

    }
    Data.append("id", id);
    $.ajax({
        url: U + 'ajax/ChouseGift',
        data: Data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function (response) {

            $(".PopUp").html($("#FinalStartTemplate").html());
        }

    });
});
$(document).on("click", "#FinalStartbut", function () {
    var Data = new FormData()
    Data.append("id", -1);
    $.ajax({
        url: U + 'ajax/StartGame',
        data: Data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function (response) {

            var Html = $("#QestionTemplate").html();
            Html = Html.replace("{Text}", response.Text);
            Html = Html.replace("{count}", response.Counter);
            var $PopUp = $(".PopUp");
            $PopUp.hide();
            $PopUp.html(Html);
            for (var i in response.Unswear) {
                var Template = $("#UnswearButTemplate").html();
                Template = Template.replace("{id}", response.Unswear[i].id);
                Template = Template.replace("{Text}", response.Unswear[i].Text);
                $(".Unswearswrapper").append(Template);
            }
            $PopUp.show();
            StartCount();

        }

    });
});
$(document).on("click", ".Unswear", function () {
    Reset()
    $("#Timer").html("15");
    $(this).off("click");
    var unswearid = $(this).data("id");
    var Data = new FormData();
    Data.append("id", unswearid);
    $.ajax({
        url: U + 'ajax/Game',
        data: Data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function (response) {

            if (response.Status) {

                $(".QestionHead").html(response.Data.Text);
                $(".QestionQounter").html(response.Data.Counter);
                $(".Unswearswrapper").html("");
                for (var i in response.Data.Unswear) {
                    var Template = $("#UnswearButTemplate").html();
                    Template = Template.replace("{id}", response.Data.Unswear[i].id);
                    Template = Template.replace("{Text}", response.Data.Unswear[i].Text);
                    $(".Unswearswrapper").append(Template);
                   
                }
                StartCount();

            }
            else {
                $(".PopUp").remove();
                var html = $("#FinishTemplate").html();
                html = html.replace("{Points}", response.Data.Score);
                html = html.replace("{min}", response.Data.Minutes);
                html = html.replace("{Sec}", response.Data.Secondes);
                $("body").append(html);
            }
        }

    });

});
$(document).on("click", "#ShowRules", function () {
    $(".PopUpBack").show();
    var html = $("#RulesTemplate").html();
    $("body").append(html);
});
$(document).on("click", "#RulesStart", function () {
    $(".PopUp").remove();
    $("#Start").click();
});
$(document).on("click", ".Videoluncher", function () {
    $(".PopUp").remove();
    var $this = $(this);
    var Template = $("#VideoTemplate").html();
    var Url = $this.data("url");
    //var Name = $this.data("name");
    //Template = Template.replace("{Name}", Name);
    Template = Template.replace("{Url}", Url);
    $(".PopUpBack").show();
    $("body").append(Template);

});
$(document).on("click", ".AboutProject", function () {
    $(".PopUpBack").show();
    var html = $("#AboutTemplate").html();
    $("body").append(html);
});
$(document).on("click", "#ShowGifts", function () {
    $(".PopUpBack").show();
    var html = $("#AboutGifts").html();
    $("body").append(html);

});
$(document).on("click", "#FirstButton", function ()
{
    $(".PopUpBack").hide();
    $(".PopUp").remove();
});
$(document).on("click", "#showRating", function ()
{
    var html = $("#Results").html();
    $(".PopUpBack").show();
    $("body").append(html);
    $(".ResultBody").perfectScrollbar();

});
$(document).on("change", "#SearchFild", function ()
{
    var Data = new FormData()
    Data.append("Search", $(this).val());
    $.ajax({
        url: U + 'ajax/Score',
        data: Data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
           
            var FirsRow = $("#TableHeadTemplate").html();
            $(".TableResults").html(FirsRow);
            for(var i in response)
            {
                var html = $("#ResulteTemplate").html();
                html = html.replace("{Name}", response[i].Name);
                html = html.replace("{Score}", response[i].Score);
                html = html.replace("{Date}", response[i].Date);
                html = html.replace("{Min}", response[i].Min);
                html = html.replace("{Sec}", response[i].Sec);
                $(".TableResults").append(html);
            }

        }

    });
});
$(document).on("click", ".LinkToRules", function ()
{
    $(".PopUp").remove();
    var html = $("#RulesTemplate").html();
    $("body").append(html);
});




function Ajaxstart(pointer) {
    pointer.parent().find(".ajaxok").hide();
    pointer.parent().find(".ajaxloader").show();
}
function Ajaxfinish(pointer) {
    pointer.parent().find(".ajaxloader").hide();
    pointer.parent().find(".ajaxok").show();
}


var int;
function StartCount()
{
    CountDown(15);
}
function Reset()
{
    clearInterval(int);
    $("#Timer").html("15");
}
function SendRandom()
{
    Reset();
    var unswearid = $(".Unswear").first().data("id");
    var Data = new FormData();
    Data.append("id", unswearid);
    $.ajax({
        url: U + 'ajax/Game',
        data: Data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function (response) {

            if (response.Status) {

                $(".QestionHead").html(response.Data.Text);
                $(".QestionQounter").html(response.Data.Counter);
                $(".Unswearswrapper").html("");
                for (var i in response.Data.Unswear) {
                    var Template = $("#UnswearButTemplate").html();
                    Template = Template.replace("{id}", response.Data.Unswear[i].id);
                    Template = Template.replace("{Text}", response.Data.Unswear[i].Text);
                    $(".Unswearswrapper").append(Template);
                 
                }
                StartCount();

            }
            else {
                $(".PopUp").remove();
                var html = $("#FinishTemplate").html();
                html = html.replace("{Points}", response.Data.Score);
                html = html.replace("{min}", response.Data.Minutes);
                html = html.replace("{Sec}", response.Data.Secondes);
                $("body").append(html);
            }
        }

    });
}
function CountDown(i) {
    int = setInterval(function () {
        $("#Timer").html(i);
        if (i == 0) {
            SendRandom();
        }
        i-- || clearInterval(int);  //if i is 0, then stop the interval
    }, 1000);
}
/*** For Responsive ***/
$('.mobile-navbar-icon').click(function () {
    $('.header').toggleClass('mobile-navbar-opened');
})
$(document).on('click', function (e) {
    e.target = $(e.target);
    if ($(e.target).closest('.pcnavbar').length == 0 && $(e.target).closest('.mobile-navbar-icon').length == 0) {
        $('.header').removeClass('mobile-navbar-opened');
    }
})