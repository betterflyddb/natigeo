﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatiGeo.Models
{
    class JsonResponse
    {
       public  bool Status { get; set; }
       public object Data { get; set; }
        public JsonResponse(object Data,bool Status=false)
        {
            this.Data = Data;
            this.Status = Status;
        }
    }
}
