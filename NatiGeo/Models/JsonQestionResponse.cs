﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatiGeo.Models.DAL;

namespace NatiGeo.Models
{
    public  class JsonQestionResponse
    {
        public int Counter { get; set; }
        public string Text { get; set; }
        public unswear[] Unswear { get; set; }
        public JsonQestionResponse(Qestion Qest)
        {
            List<unswear> Temp = new List<unswear>();
            Text = Qest.text;
            foreach(var i in Qest.Unswears)
            {
                Temp.Add(new unswear()
                {
                    id = i.id,
                    Text= i.text
                
                });
            }
            Unswear = Temp.ToArray();
        }
        
    }
    public class unswear
    {
      public  int id { get; set; }
      public string Text { get; set; }
    }
}
