﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatiGeo.Models
{
   public  class FinalReturnModel
    {
        public int Minutes { get; set; }
        public int Secondes { get; set; }
        public int Score { get; set; }
    }
}
