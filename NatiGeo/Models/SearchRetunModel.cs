﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatiGeo.Models.DAL;
namespace NatiGeo.Models
{
   public class SearchRetunModel
    {
        public int Score { get; set; }
        public string Min { get; set; }
        public string Sec { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public SearchRetunModel(User input)
        {
            Name = input.Name;
            Score = input.Score.Value;
            Date = input.GameStartTime1.Value.ToString("yyyy/MM/dd");           
            Min = (input.GameFinishTime1 - input.GameStartTime1).Value.Minutes.ToString();
            if(int.Parse(Min)<10)
            {
                Min = "0" + Min;
            }
            Sec = (input.GameFinishTime1 - input.GameStartTime1).Value.Seconds.ToString();
        }
    }
}
