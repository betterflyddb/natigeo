﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatiGeo.Models
{
   public class APiResponse
    {
		public bool status { get; set; }
		public int ErorrCode { get; set; }
		public APiResponse(string[] array)
        {
            status = Convert.ToBoolean(array[0]);
            ErorrCode = Convert.ToInt32(array[1]);
        }
        public APiResponse()
        {

        }
    }
}
