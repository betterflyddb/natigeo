﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatiGeo.Models.DAL;


namespace NatiGeo.Models
{
   public  class UnitiOfWork
    {
        private storyofg_Entities _context;
        public storyofg_Entities context
        {
            get
            {
                if (_context == null)
                    _context = new storyofg_Entities();

                return _context;
            }
           
        }
        public void Save()
        {
            context.SaveChanges();
        }
    }
}
