﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NatiGeo.Models;

namespace NatiGeo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private UnitiOfWork db = new UnitiOfWork();
        public ActionResult Index()
        {
           // ViewBag.SortedUsers = db.context.Users.OrderByDescending(x => x.Score).ThenBy(x => x.TotalGameSeConds).ToArray().Take(10);
            return View();
        }
        public ActionResult Score()
        { 
            var SortedUsers= db.context.Users.Where(x=>x.Score!=null).OrderByDescending(x => x.Score).ThenBy(x => x.TotalGameSeConds).ToArray();
            SortedUsers = SortedUsers.Where(x => x.Score != 0).ToArray();
            var List = SortedUsers.ToList();
            var Last = List.FirstOrDefault(x => x.id == 222);
            List.Remove(Last);
            List.Add(Last);
            SortedUsers = List.ToArray();
            return View(SortedUsers);
        }
    }
}