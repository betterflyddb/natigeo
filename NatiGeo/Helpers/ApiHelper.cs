﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatiGeo.Models;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace NatiGeo.Helpers
{
   public static class ApiHelper
    {
        public static APiResponse GetInfo(string SilkToken,string IdNumber)
        {
            string Url = string.Format("https://mw.silknet.com/NatGeoPromo/users/{0}/account/{1}", IdNumber, SilkToken);
            var request = WebRequest.Create(Url);
            request.ContentType = "application/json; charset=utf-8";
            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string[] info = jss.Deserialize<string[]>(text);
            return new  APiResponse(info);
        }
    }
}
